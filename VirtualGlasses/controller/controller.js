let showKinh = (id) => {
  for (var index = 0; index < dataGlasses.length; index++) {
    var glass = dataGlasses[index];
    if (glass.id == id) {
      deoKinh(glass.virtualImg);
      thongTinKinh(glass.item);
    }
  }
};

let deoKinh = (virtualImg) => {
  const node = `<img src=${virtualImg} alt="" />`;
  document.getElementById("avatar").innerHTML = node;
};

let thongTinKinh = (item) => {
  const info = `
       <div>
        <h3>${item.name} - ${item.brand} (${item.color})</h3>;
        <span>${item.price}$</span>;
        <p>${item.description}</p>;
       </div>
         `;
  document.getElementById("glassesInfo").innerHTML = info;
};
